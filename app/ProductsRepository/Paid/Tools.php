<?php

namespace App\ProductsRepository\Paid;

use Illuminate\Http\Request;
use App\Product;
use Illuminate\Support\Facades\DB;
use Illuminate\Routing\Redirector;
use App\Tool;

class Tools{

    function __construct() {
       
    }


    public static function get($productId){
        
        $tools =  Tool::where('product_id',$productId)
                        ->where('free', 0)->get();
        return $tools;

    }

    

}

