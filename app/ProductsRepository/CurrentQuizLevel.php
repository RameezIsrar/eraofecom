<?php

namespace App\ProductsRepository;

use Illuminate\Http\Request;
use App\Product;
use Illuminate\Support\Facades\DB;
use Illuminate\Routing\Redirector;
use App\Quiz;
use Illuminate\Support\Facades\Auth;
use App\UserTrainingHistory;

class CurrentQuizLevel{

    function __construct() {
       
    }


    public static function get($productId){
        
        $user_id = Auth::id();

        $current_quest_level_of_the_user_on_the_product = UserTrainingHistory::where('user_id',$user_id)
                                                        ->where('product_id',$productId)
                                                        ->max('quest_level');
                                                        
          
        return $current_quest_level_of_the_user_on_the_product;
    }


}



