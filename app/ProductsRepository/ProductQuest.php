<?php

namespace App\ProductsRepository;

use Illuminate\Http\Request;
use App\Product;
use Illuminate\Support\Facades\DB;
use Illuminate\Routing\Redirector;
use App\Quiz;

class ProductQuest{

    function __construct() {
       
    }


    //get the quest id of the product
    //default the quest level is 1.1// if not passed then the default is 1.1 quest level
    public static function Id($productId,$questId = 1.1){
        
        $questId = Quiz::where('level', $questId)
                      ->where('product_id',$productId) //here the return value is actually the level i.e 1.2,1.3 of the quiz id from the quiz table
        ->pluck('id');
        return $questId[0];
    }


}



