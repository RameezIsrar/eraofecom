<?php

namespace App\Http\Controllers\FreeTrial;

use Illuminate\Http\Request;
use App\Tool;
use App\Product;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\DB;
use Laravel\Cashier\Subscription;
use App\AllAccess\SubscribedAllAccess;
use App\ProductsRepository\PublishedProducts;
use App\SubscriptionRepository\UserSubscriptions;
use App\ProductsRepository\SubscribedProduct;
use App\Tasks\IsUnderFreeTrial;
use App\ProductsRepository\FreeTrial\Tools as FreeTools;
use App\ProductsRepository\Paid\Tools as PaidTools;

class ToolController extends Controller
{
    private $allAccess;
    
    private $products;

    private $userSubscriptions;

    private $isSubscribedProduct;

    public function __construct(
                                SubscribedAllAccess $allAccess,
                                PublishedProducts $products, 
                                UserSubscriptions $userSubscriptions,
                                SubscribedProduct $isSubscribedProduct
                                )
    {
        $this->middleware('auth');
        
        $this->allAccess = $allAccess; // all access subscription

        $this->products = $products;

        $this->userSubscriptions = $userSubscriptions;

        $this->isSubscribedProduct = $isSubscribedProduct;
    }

    public function presentation($name){
        
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user_id = Auth::id();
        $products = $this->products->getAllPublishedProducts(); // all active/published products
        
        $isAllAccessSubscribed = $this->allAccess->getAllPoductsOnAccessAll(); // either false or list of all products if subscribed all access
       
        // //check first for all access subscription
        if(($isAllAccessSubscribed) != false){
           
            return view('free.tool.index',[
                'products' => $isAllAccessSubscribed
            ]);
        } // return as the user has subscribed to all access

        $subscribed_products = $this->userSubscriptions->subscribed_products(); // for multiple susbcrition
      
        if(($subscribed_products) != ''){

            return view('free.tool.index',[
                'products' => $subscribed_products
            ]);
        }

        if($subscribed_products[0] == false){ // user has unsubscribed and grce period ends too
            return redirect('settings#/subscription'); 
        }else{
            return view('free.tool.index',[
                'products' => $subscribed_products
            ]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $isUnderFreeTrial  = isUnderFreeTrial::verify();

        $tools =  FreeTools::get($id); // get the free tools of the product

        $paidTools =  PaidTools::get($id); // get the free tools of the product
       

        $licence = Product::where('id', $id)->pluck('name');
        
    
        if($isUnderFreeTrial){
            return view('free.tool.show',[
                'tools' => $tools,
                'paidTools' => $paidTools,
                'licence' => $licence[0]
            ]);
        }else{
            return redirect('settings#/subscription'); 
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


}
