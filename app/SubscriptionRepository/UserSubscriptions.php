<?php

namespace App\SubscriptionRepository;

use App\Product;
use Illuminate\Support\Facades\Auth;
use Laravel\Cashier\Subscription;
use App\Plan;

class UserSubscriptions
{

    public function __construct()
    {

    }

    public function subscribed_products()
    { // get the user's subcribed products

        $user_id = Auth::id();

        $current_billing_plans = Subscription::where('user_id', $user_id)->where('ends_at', null)->pluck('stripe_plan');
        if ($current_billing_plans != null) {
            $subscribed_products = [];

            $subscribed_products = Product::where('publish', 1)->get();

            return $subscribed_products;
        } else {

            return false;
        }
    }


    public static function products(){ // get the user's subcribed products

        $user_plan = Auth::user()->current_billing_plan; // user subscribed plan
      

        // now get the zone id for the subscribed plan
        $plan_includes = Plan::where('plan_id',$user_plan)->pluck('includes')->first();
        
        $subscribed_products = [];
      

        if($plan_includes == "all"){ // if the user subscribed to pro plan
        
            //now get all the products of that  plan zone
            $subscribed_products = Product::where('publish',1)->get();

            return $subscribed_products;
       
        }else{
            
            
            return false; // not subscribed to any plan
        
        }
        
    }  

}
