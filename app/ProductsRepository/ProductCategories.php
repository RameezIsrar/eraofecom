<?php

namespace App\ProductsRepository;

use App\Product;

use App\CourseCategory;
use App\ProductsRepository\FirstQuizId;

class ProductCategories
{

    public function __construct()
    {

    }

    public static function formatted()
    {

        $CourseCategory = CourseCategory::where('publish', 1)->get();

    
        foreach ($CourseCategory as $firstKey => $firstValue) {

            

            $CourseCategory[$firstKey]['icon'] = 'keyboard_arrow_up';

            $CourseCategory[$firstKey]['icon-alt'] = 'keyboard_arrow_down';

            $CourseCategory[$firstKey]['textIcon'] = 'card_giftcard';

            $CourseCategory[$firstKey]['textIcon-alt'] = 'card_giftcard';

            $CourseCategory[$firstKey]['text'] = $firstValue['name'];

            $CourseCategory[$firstKey]['model'] = false;

            $CourseCategory[$firstKey]['children'] = '';

            $licences = Product::where('publish', 1)
                                ->where('course_category_id', $firstValue['id'])
                                ->get();
            $subchildren = [];
         
            foreach ($licences as $key => $value) {

                $licences[$key]['text'] = $value['name']; // adding a new key with value same as of title for the sidebar navigation

                $licences[$key]['seal'] = $value['picture'];

                $licences[$key]['modal'] = false;

                $subchildren[0]['text'] = 'Training';

                $firstQuizID = FirstQuizId::get($value['id']);

                $subchildren[0]['url'] = 'training/' . $value['id'] . '/' . $firstQuizID;

                $subchildren[0]['icon'] = 'assignment';

                $subchildren[1]['text'] = 'Tools';

                $subchildren[1]['url'] = 'tools/' . $value['id'];

                $subchildren[1]['icon'] = 'picture_as_pdf';

                $subchildren[2]['text'] = 'Branding';

                $subchildren[2]['url'] = 'branding/' . $value['id'];

                $subchildren[2]['icon'] = 'branding_watermark';

                $licences[$key]["subChildren"] = $subchildren;
            }
            $CourseCategory[$firstKey]['children'] = $licences;

        }

      
        return $CourseCategory;

    }

}
