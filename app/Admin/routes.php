<?php

use Illuminate\Routing\Router;

Admin::registerAuthRoutes();

Route::group([
    'prefix'        => config('admin.route.prefix'),
    'namespace'     => config('admin.route.namespace'),
    'middleware'    => config('admin.route.middleware'),
], function (Router $router) {

    $router->get('/', 'HomeController@index');
    
    
    $router->post('/auth/products/store', 'ProductController@store');
    $router->post('/auth/products/{id}/update', 'ProductController@update');
    $router->resource('/auth/products', 'ProductController');
    
    $router->post('/auth/tools/store', 'ToolController@store');
    $router->post('/auth/tools/{id}/update', 'ToolController@update');
    $router->resource('/auth/tools', 'ToolController');

    $router->post('/auth/branding/store', 'BrandingController@store');
    $router->post('/auth/branding/{id}/update', 'BrandingController@update');
    $router->resource('/auth/branding', 'BrandingController');

    $router->post('/auth/categories/store', 'WorkoutCategoryController@store');
    $router->post('/auth/categories/{id}/update', 'WorkoutCategoryController@update');
    $router->resource('/auth/categories', 'WorkoutCategoryController');

    $router->post('/auth/scripts/store', 'WorkoutScriptController@store');
    $router->post('/auth/scripts/{id}/update', 'WorkoutScriptController@update');
    $router->resource('/auth/scripts', 'WorkoutScriptController');

    $router->post('/auth/product_categories/store', 'ProductWorkoutCategoryController@store');
    $router->post('/auth/product_categories/{id}/update', 'ProductWorkoutCategoryController@update');
    $router->resource('/auth/product_categories', 'ProductWorkoutCategoryController');


    $router->post('/auth/quizzes/store', 'QuizController@store');
    $router->post('/auth/quizzes/{id}/update', 'QuizController@update');
    $router->resource('/auth/quizzes', 'QuizController');

    $router->post('/auth/questions/store', 'QuestionController@store');
    $router->post('/auth/questions/{id}/update', 'QuestionController@update');
    $router->resource('/auth/questions', 'QuestionController');

    $router->resource('/auth/subscriptions', 'SubscriptionController');

    $router->resource('/auth/customers', 'CustomerController');

    $router->post('/auth/plans/store', 'PlanController@store');
    $router->post('/auth/plans/{id}/update', 'PlanController@update');
    $router->resource('/auth/plans', 'PlanController');
    
    $router->post('/auth/plan_categories/store', 'PlanCategoryController@store');
    $router->post('/auth/plan_categories/{id}/update', 'PlanCategoryController@update');
    $router->resource('/auth/plan_categories', 'PlanCategoryController');

    $router->resource('/auth/score', 'ScoreController');


    $router->post('/auth/course_categories/store', 'CourseCategoryController@store');
    $router->post('/auth/course_categories/{id}/update', 'CourseCategoryController@update');
    $router->resource('/auth/course_categories', 'CourseCategoryController');

    $router->post('/auth/levels/store', 'LevelController@store');
    $router->post('/auth/levels/{id}/update', 'LevelController@update');
    $router->resource('/auth/levels', 'LevelController');

    $router->post('/auth/question_options/store', 'QuestionOptionsEqualController@store');
    $router->post('/auth/question_options/{id}/update', 'QuestionOptionsEqualController@update');
    $router->resource('/auth/question_options', 'QuestionOptionsEqualController');
    
});
