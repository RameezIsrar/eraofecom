<?php

namespace App\ProductsRepository;

use App\Level;
use App\Quiz;
use App\QuizRepository\Locked;
use App\QuizRepository\Passed;

class LevelsJson
{

    public function __construct()
    {

    }

    public static function formatted($productId)
    {
      
        $product_levels = Level::where('product_id', $productId)->get();
        $passed_quests = Passed::formatted($productId); // call this in order to update the $count property of Passesd Class


        //dd($product_levels);
        $locked_quests = Locked::formatted(Passed::$count);

       // dd($locked_quests);

        $count = 0;
        foreach ($product_levels as $key => $value) {


            $product_levels[$key]['details'] = $value['total_videos'] . ' videos | Total ' . $value['total_duration'];

            $quiz_details = Quiz::where('level_id', $value['id'])->get();

            $product_levels[$key]['sectionDetails'] = $quiz_details;

            foreach ($quiz_details as $secondKey => $secondValue) {

                $product_levels[$key]['sectionDetails'][$secondKey]['quizId'] = $secondValue['id'];

                $product_levels[$key]['sectionDetails'][$secondKey]['sectionTitle'] = $secondValue['intro'];

                $product_levels[$key]['sectionDetails'][$secondKey]['activeSection'] = false;

                $product_levels[$key]['sectionDetails'][$secondKey]['completed'] = $passed_quests[$count];

                $product_levels[$key]['sectionDetails'][$secondKey]['isLocked'] = $locked_quests[$count];

                $count++;
            }

        }

        return $product_levels;
    }

}
