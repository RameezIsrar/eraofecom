@extends('master')
@section('title', 'Tools')

@section('page-title')
  @yield('title')
@endsection

@section('page-content')
          <!-- =============== training-body =============== -->
          @if(count($products) > 0)
        <div class="row">
          <div class="col-sm-6">
            <div class="card" >
              <img class="card-img-top" src='{{asset("img/healthy_body.png")}}' alt="Card image cap">
              <div class="card-body">
                <h5 class="card-title">A Healthy Body</h5>
                <a href="https://s3.us-east-2.amazonaws.com/eraofecom1/tools/healthy_body.pptx" class="btn btn-primary">Download Now</a>
              </div>
            </div>
          </div>

          <div class="col-sm-6">
            <div class="card">
              <img class="card-img-top" src='{{asset("img/healthy_mindset.png")}}' alt="Card image cap">
              <div class="card-body">
                <h5 class="card-title">A Healthy Mindset</h5>
                <a href="https://s3.us-east-2.amazonaws.com/eraofecom1/tools/healthy_mind.pptx" class="btn btn-primary">Download Now</a>
              </div>
            </div>
          </div>
        </div>

        <br />

        <h4>Your Tools</h4>

        <div class="row">  
            @foreach ($products as $product)
                <div class="col-sm-4">
                  <div class="card">
                    <img class="card-img-top" src='{{asset("storage/$product->picture")}}' alt="Card image cap">
                    <div class="card-body">
                      <h5 class="card-title">{{$product->name}}</h5>
                      <a href="tools/{{$product->id}}" class="btn btn-primary">Start Now</a>
                    </div>
                  </div>
                </div>
            @endforeach

        </div>

         @else

        <div class="row">
            <div class="container">
              <p>You havent subscribed to any of the Licence or Designation yet. Click on the button to get started now.</p>
              <a href="settings#/subscription" class="btn btn-primary btn-lg active" role="button" aria-pressed="true">Subscribe Now</a>
            </div>
        </div>

        @endif
          <!-- =============== training-body ended=============== -->
@endsection
