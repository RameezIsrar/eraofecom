@extends('spark::layouts.extended')

@section('content')

@inject('IsUnderFreeTrial', 'App\Tasks\IsUnderFreeTrial')

<home :user="user" inline-template>
</home>


    <div class="container" style="max-width:1400px">
        <!-- Application Dashboard -->
        <div class="row justify-content-center">
            <!-- ================ sidbar ================== -->
            <!-- <div class="col-md-3">
                <div class="card card-default">
                    <div class="card-header"><a class="nav-link" href="{{ $IsUnderFreeTrial->verify() }}/home">{{__('Dashboard')}}</a></div>

                    <div class="card-body">

                          <ul class="nav flex-column">
                            <li class="nav-item">
                                <a class="nav-link" href="{{ $IsUnderFreeTrial->verify() }}/training">Training</a>
                            </li> -->
                            <!-- <li class="nav-item">
                              <a class="nav-link" href="/workouts">Workouts</a>
                            </li> -->
                            <!-- <li class="nav-item">
                              <a class="nav-link" href="{{ $IsUnderFreeTrial->verify() }}/tools">Tools</a>
                            </li>
                            <li class="nav-item">
                              <a class="nav-link" href="{{ $IsUnderFreeTrial->verify() }}/branding">Branding</a>
                            </li>
                          </ul>
                    </div>
                </div>
            </div> -->

            <!-- ================ sidebar ended ============= -->
            <div class="col-md-12">
                <div class="card card-default">
                    <div class="card-header">
                   
                    </div>

                    <div class="card-body">

                        <div class="pit" v-cloak>
                          
                          <v-app>
                              <app-header></app-header>
                          </v-app>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>


<script>
    // $(document).ready(function(){
    //     var current = location.pathname;
    //     result= current.split('/');
    //     console.log(result[1]);
    //     $('.card .nav li a').each(function(){
    //         var $this = $(this);
    //         //console.log($this);
    //         // if the current path is like this link, make it active
    //         if($this.attr('href').indexOf(result[1]) !== -1){
    //             $this.addClass('active');
    //         }
    //     });
    // });
</script>

<link rel="stylesheet" href="{{ mix('/css/ui.css') }}">

<script src="{{ mix('/js/custom-app.js')}}"></script>

<script src="https://cdn.jsdelivr.net/npm/vuetify/dist/vuetify.js"></script>


@endsection
