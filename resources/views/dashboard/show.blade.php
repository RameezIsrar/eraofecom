@extends('master')
@section('title', 'Your ' . $product->name . '  Course Progress')

@section('page-title')
  @yield('title')
@endsection

@inject('IsUnderFreeTrial', 'App\Tasks\IsUnderFreeTrial')

@section('page-content')
          <!-- =============== training-body =============== -->
        
        @if($progress == 100)
        <div class="alert alert-success" role="alert">
            <h4 class="alert-heading">Well done!</h4>
            <p>You have completed the training phase successfully. You are now better than yesterday.</p>
            <hr>
            <p class="mb-0">Congratulations! You have unlocked your branding. Click <a href="../branding/{{$product->id}}" class="btn btn-primary btn-lg active" role="button" aria-pressed="true">Branding</a> to get your certificate.</p> 
        </div>
        @elseif($progress > 1 && $progress < 100)
        <div class="alert alert-info" role="alert">
            <h4 class="alert-heading">Good Job!</h4>
            <p>You have done a good job so far. Keep going. The training session will make you a better Health Professional.</p>
            <hr>
            <p class="mb-0">Few more training phases are left to complete. We know that you can do this.</p>
        </div>
        @else
        <div class="alert alert-warning" role="alert">
            You haven't started your training yet <a href="{{ $IsUnderFreeTrial->verify() }}/training" class="alert-link">Click Here to begin your training program</a>. Good Luck!
        </div>
        @endif

        <div class="row">
            <div class="col-md-4 col-sm-12">
                <div class="card">
                    <img class="card-img-top" src='{{asset("storage/$product->picture")}}' alt="Card image cap">
                    <div class="card-body">
                        <h5 class="card-title">{{$product->name}}</h5>
                        @if($progress > 1)
                        <div class="progress">
                            <div class="progress-bar bg-success" role="progressbar" @if($progress == 6)  style="width: 7%" @else style="width: {{$progress}}%" @endif aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">{{$progress}}%</div>
                        </div>
                        @endif
                    </div>
                </div>
            </div>
     

        <div class="col-md-6 col-sm-12 score">
        <h5>Score Board</h5>
                <div class="row">
                   
                    <div class="col-md-6">
                        <ul class="list-group">
                            <li class="list-group-item">Phase 1.1 @if($score_array[0] > 0) : {{$score_array[0]}}%    @endif</li>
                            <li class="list-group-item">Phase 1.2 @if($score_array[1] > 0) : {{$score_array[1]}}%    @endif</li>
                            <li class="list-group-item">Phase 1.3 @if($score_array[2] > 0) : {{$score_array[2]}}%    @endif </li>
                            <li class="list-group-item">Phase 1.4 @if($score_array[3] > 0) : {{$score_array[3]}}%    @endif</li>
                
                            <li class="list-group-item ">Phase 2.1 @if($score_array[4] > 0) : {{$score_array[4]}}%    @endif</li>
                            <li class="list-group-item">Phase 2.2 @if($score_array[5] > 0) : {{$score_array[5]}}%    @endif</li>
                            <li class="list-group-item">Phase 2.3 @if($score_array[6] > 0) : {{$score_array[6]}}%    @endif</li>
                            <li class="list-group-item">Phase 2.4 @if($score_array[7] > 0) : {{$score_array[7]}}%    @endif</li>

                        </ul>  
                    </div>
                    <div class="col-md-6">
                        <ul class="list-group">
                            
                            <li class="list-group-item ">Phase 3.1 @if($score_array[8] > 0) : {{$score_array[8]}}%    @endif</li>
                            <li class="list-group-item">Phase 3.2 @if($score_array[9] > 0) : {{$score_array[9]}}%    @endif</li>
                            <li class="list-group-item">Phase 3.3 @if($score_array[10] > 0) : {{$score_array[10]}}%    @endif</li>
                            <li class="list-group-item">Phase 3.4 @if($score_array[11] > 0) : {{$score_array[11]}}%    @endif</li>

                            <li class="list-group-item ">Phase 4.1 @if($score_array[12] > 0) : {{$score_array[12]}}%    @endif</li>
                            <li class="list-group-item">Phase 4.2 @if($score_array[13] > 0) : {{$score_array[13]}}%    @endif</li>
                            <li class="list-group-item">Phase 4.3 @if($score_array[14] > 0) : {{$score_array[14]}}%    @endif</li>
                            <li class="list-group-item">Phase 4.4 @if($score_array[15] > 0) : {{$score_array[15]}}%    @endif</li>

                        </ul>  
                    </div>
                </div>
            </div>
          <!-- =============== training-body ended=============== -->
@endsection
