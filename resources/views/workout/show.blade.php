@extends('master')
@section('title', $licence . ' Workout Categories')
@section('page-title')
  @yield('title')
@endsection


@section('page-content')

@push('scripts')

  <div class="row">

  @foreach ($categories as $category)    
  <?php //var_dump($category[0]['title']);?>
      @foreach ($category as $key => $value) 
       
     
    <div class="col-sm-4">
      <div class="card">
      <img class="card-img-top" src="{{asset("storage/$value->image")}}" alt="Card image cap">
        <div class="card-body">
          <h5 class="card-title">{{ $value->title}}</h5>
          <a href="{{$product_id}}/{{$value->id}}" class="btn btn-primary">Show All</a>
        </div>
      </div>
    </div>
    @endforeach
  @endforeach

  </div>

@endsection
