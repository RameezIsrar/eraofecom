<?php

namespace App\Admin\Controllers;

use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;
use App\Quiz;
use App\Question;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector;
use App\Option;

class QuestionController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        
        return Admin::content(function (Content $content) {

            $content->header('Questios');
            $content->description('List');

            $content->body($this->grid());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        // return Admin::content(function (Content $content) use ($id) {

        //     $content->header('header');
        //     $content->description('description');

        //     $content->body($this->form()->edit($id));
        // });

        $question= Question::where('id', $id)->get();
        $options = json_decode($question[0]->options);
        $quizzes= Quiz::all(); 
        
        return view('vendor/admin/questions.edit',[
            'question' => $question[0],
            'options' => $options,
            'quizzes' => $quizzes
        ]);

    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        // return Admin::content(function (Content $content) {

        //     $content->header('header');
        //     $content->description('description');

        //     $content->body($this->form());
        // });

        //alpha
        $quizzes= Quiz::all();
         
        return view('vendor/admin/questions.create',[
            'quizzes' => $quizzes
        ]);

    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(Question::class, function (Grid $grid) {

            $grid->id('ID')->sortable();
            $grid->text('Text');
            $grid->answer('Answer');

            $grid->quiz_id()->display(function($quiz_id) {
                return Quiz::find($quiz_id)->title;
            });
            
            $grid->created_at();
            $grid->updated_at();

            $grid->filter(function ($filter) {

                // Sets the range query for the created_at field
                $filter->between('created_at', 'Created Time')->datetime();

                $filter->equal('quiz_id','Quiz ID');
            });

            $grid->filter(function ($filter) {
                
                $filter->equal('ID','Question ID');
            });

            $grid->filter(function ($filter) {

                $filter->like('text','Question Text');
            });
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(Question::class, function (Form $form) {

            $form->display('id', 'ID');

            $form->text('name', 'Name');
            $form->display('created_at', 'Created At');
            $form->display('updated_at', 'Updated At');
        });
    }

    public function store(Request $request){
     

        $validatedData = $request->validate([
            'text' => 'required',
            'quiz_id'  => 'required',
            'answer'  => 'required',
            'options.*' => 'required|min:4' // make sure all the 4 options have been provided
        ]);

            $options=$request->options;

           

            $array_options =[
                'A' => $request->options[0],
                'B' => $request->options[1],
                'C' => $request->options[2],
                'D' => $request->options[3],
            ] ;

            
               
            $Question = new Question;
            $Question->text = $request->text;
            $Question->quiz_id = $request->quiz_id;
            $Question->options = json_encode($array_options);
            $Question->answer= $request->answer;
            
            $Question->save(); 

            // save the question options now with newly added question ID
            foreach ($array_options as $key => $value) {

                $option = new Option;

                $option->option = $value ;
                $option->value = $key;
                $option->question_id = $Question->id ;// last inserte question model id
                $option->save();

            }

            return redirect('/admin/mamango=1/auth/questions');
           
       
    }

    public function update(Request $request, $id){
       
        $validatedData = $request->validate([
            'text' => 'required',
            'quiz_id'  => 'required',
            'answer'  => 'required',
            'options.*' => 'required|min:4'
        ]);

            $Question = Question::find($id);
            $options=$request->options;
            $array_options =[
                'A' => $request->options[0],
                'B' => $request->options[1],
                'C' => $request->options[2],
                'D' => $request->options[3],
            ] ;

            $Question->text = $request->text;
            $Question->quiz_id = $request->quiz_id;
            $Question->options = json_encode($array_options);
            $Question->answer= $request->answer;
            
            $Question->save();
            return redirect('/admin/mamango=1/auth/questions');
            
        
    }

    public function destroy($id){

        $deletedRows = Question::where('id', $id)->delete();

        $deletedRows = Option::where('question_id', $id)->delete();
      
        return 'success';
        
    }
}
