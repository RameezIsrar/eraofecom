<?php

namespace App\Admin\Controllers;

use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;
use App\WorkoutCategory;
use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector;
use App\WorkoutScript;
use App\ProductWorkoutCategory;
use App\ProductsRepository\AllProducts;


class WorkoutCategoryController extends Controller
{
    use ModelForm;

    private $products;

    public function __construct(AllProducts $products)
    {

        $this->products = $products;
        
    }

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        
        return Admin::content(function (Content $content) {

            $content->header('Workout Categories');
            $content->description('List');

            $content->body($this->grid());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        // return Admin::content(function (Content $content) use ($id) {

        //     $content->header('header');
        //     $content->description('description');

        //     $content->body($this->form()->edit($id));
        // });

        $workout_category= WorkoutCategory::where('id', $id)->get();
        return view('vendor/admin/workoutcategories.edit',[
            'workout_category' => $workout_category[0],
        ]);

    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        // return Admin::content(function (Content $content) {

        //     $content->header('header');
        //     $content->description('description');

        //     $content->body($this->form());
        // });

        $products= $this->products->allProducts(); 
         
        return view('vendor/admin/workoutcategories.create',[
            'products' => $products
        ]);

    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(WorkoutCategory::class, function (Grid $grid) {

            $grid->id('ID')->sortable();
            $grid->title('Title');
            $grid->created_at();
            $grid->updated_at();
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(Tool::class, function (Form $form) {

            $form->display('id', 'ID');
            $form->text('name', 'Name');
            $form->display('created_at', 'Created At');
            $form->display('updated_at', 'Updated At');
        });
    }

    public function store(Request $request){
        if ($request->has('file')) {
            $workoutCategory = new WorkoutCategory;
            $workoutCategory->title = $request->title;
            $workoutCategory->image= $request->file('file')->store('workout_categories','s3');
            $workoutCategory->save();
            return redirect('/admin/mamango=1/auth/categories');
        }else{
            dd('A picture is required for a Category');
        }
    }

    public function update(Request $request, $id){
        if ($request->has('file')) {
            $directories = Storage::directories('');
            
            // now update a product
            $WorkoutCategory = WorkoutCategory::find($id);
            $WorkoutCategory->title = $request->title;
            $WorkoutCategory->image = $request->file('file')->store('workout_categories','s3');
            $WorkoutCategory->save();

            return redirect('/admin/mamango=1/auth/categories');
        }else{
            $WorkoutCategory = WorkoutCategory::find($id);
            $WorkoutCategory->title = $request->title;
            $WorkoutCategory->save();
            return redirect('/admin/mamango=1/auth/categories');
        }
        
    }

     public function destroy($id){
        WorkoutCategory::destroy($id);  

        //also delete from the workout_scripts table -- also unlink audio
        $deletedRows = WorkoutScript::where('workout_category_id', $id)->delete();

        // also delete every row from the product_workout_categories table
        $deletedRows = ProductWorkoutCategory::where('workout_category_id', $id)->delete();
        return 'success';
   }
}
