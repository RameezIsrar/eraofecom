<?php

namespace App\ProductsRepository;

use App\Product;
use App\Tasks\Score;
use App\SubscriptionRepository\UserSubscriptions;
use Illuminate\Support\Facades\Auth;
use App\UserTrainingHistory;
use App\Tasks\IsUnderFreeTrial;

class CompletedCoursesScore
{

    public function __construct()
    {

    }

    public static function formatted()
    {   
        $user_id = Auth::id();

        $products = UserSubscriptions::products();

        $isUnderFreeTrial = IsUnderFreeTrial::get();

        if($isUnderFreeTrial){

            $products = PublishedProducts::get();

        }

       


        $completedTrainingProducts = UserTrainingHistory::where('quest_level',4.4)
                                                        ->where('user_id',$user_id)
                                                        ->pluck('product_id');

                                                        
       
        foreach ($products as $key => $value) {

            $exists = UserTrainingHistory::where('quest_level',4.4)
                                                        ->where('user_id',$user_id)
                                                        ->where('product_id',$value['id'])->exists();

            if($exists){
                $products[$key]['courseName'] = $value['name']; // adding a new key with value same as of title for the sidebar navigation

                $products[$key]['url'] = "/scripts/$value[id]";
            
                $products[$key]['seal'] = $value['picture'];

                $products[$key]['phase'] = [];

                $products[$key]['phase']  = Score::result($value['id']);
            
                $score  = Score::result($value['id']);
            
                $products[$key]['modal'] = true;

                $products[$key]['completed'] = true;

            }
          

        }
        return $products;

    }

}
