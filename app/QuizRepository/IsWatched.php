<?php

namespace App\QuizRepository;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\UserTrainingHistory;
use App\Product;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\DB;
use App\SubscriptionRepository\UserSubscriptions;
use App\Quiz;

class IsWatched {

    public static function verify($productId,$questId){ // get the user's subcribed products
 
         $userId = Auth::id();
         
         $already_watched = DB::table('user_training_histories')
                             ->where('user_id', $userId)
                             ->where('product_id', $productId)
                             ->where('quest_level', $questId)
                             ->where('video_watched', 1)->exists(); 
                             
         return $already_watched;
     }   
 
 
 }



