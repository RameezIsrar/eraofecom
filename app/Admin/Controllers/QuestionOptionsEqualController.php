<?php

namespace App\Admin\Controllers;

use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;
use App\Quiz;
use App\Question;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector;
use App\Option;
use App\ProductsRepository\PublishedProducts;


class QuestionOptionsEqualController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        
        return Admin::content(function (Content $content) {

            $content->header('Add Quiz questions options to the options table');
            $content->description('List');

            $content->body($this->grid());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        // return Admin::content(function (Content $content) use ($id) {

        //     $content->header('header');
        //     $content->description('description');

        //     $content->body($this->form()->edit($id));
        // });
        
        //$product= Equal::where('id', $id)->get(); 
        // return view('vendor/admin/products.edit',[
        //     'product' => $product[0],
        //      'zones' => $zones
            
        // ]);

    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        // return Admin::content(function (Content $content) {

        //     $content->header('header');
        //     $content->description('description');

        //     $content->body($this->form());
        // });

        //alpha
        $products= PublishedProducts::get();

        return view('vendor/admin/equals.create',[
            'products' => $products
        ]);

    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(Option::class, function (Grid $grid) {

            $grid->id('ID')->sortable();
            //$grid->option('option');
            $grid->question_id('question_id');
            $grid->value('value');
           
            // $grid->question_id()->display(function($question_id) {
            //    return Question::find($copy_id)->reference_name;
            // });
         
            $grid->created_at();
            $grid->updated_at();

            $grid->filter(function ($filter) {
                
                $filter->equal('question_id','Question ID');
            });

            
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(Product::class, function (Form $form) {

            $form->display('id', 'ID');

            $form->text('name', 'Name');
            $form->display('created_at', 'Created At');
            $form->display('updated_at', 'Updated At');
        });
    }

    public function store(Request $request){

        $product_id =$request->copy_id;
    
        $quizzes = Quiz::where('product_id',$product_id)->get();

        foreach ($quizzes as $quiz) {
            
            $questions = Question::where('quiz_id',$quiz->id)->get();

            foreach($questions as $question){

               // dd(json_decode($question->options));
                
               $result = Option::where('question_id',$question->id)->get();
                
               if($result->count() != 4){ // if that quesrtion options isnt entered before // or preventing double entries
                   
                    foreach(json_decode($question->options) as $thirdKey => $thirdValue){

                        

                                $option = new Option;   
                                
                                $option->option = $thirdValue;
                                $option->value = $thirdKey;
                                $option->question_id = $question->id;
                                
                                $option->save();
                        
                    
                    }
                }

            }

        }
        
       
    
    }



    public function destroy($id){
        return false; // so the product wont be able to delte from the admin
                     // if you need to make a product delte, just remove this destroy function from here.
    }
}
