<?php

namespace App\QuizRepository;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\UserTrainingHistory;
use App\Product;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\DB;
use App\SubscriptionRepository\UserSubscriptions;
use App\Quiz;

class Level {

    public static function get($questId){ // get the user's subcribed products

        $questId = Quiz::where('id', $questId) //here the return value is actually the level i.e 1.2,1.3 of the quiz id from the quiz table
        ->pluck('level');
        return $questId[0];

    }  


}



