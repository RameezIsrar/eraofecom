<?php

namespace App\Http\Controllers;

use App\Workout;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\ProductWorkoutCategory;
use App\Product;
use App\WorkoutCategory;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Illuminate\Routing\Redirector;
use Laravel\Cashier\Subscription;
use App\AllAccess\SubscribedAllAccess;
use App\ProductsRepository\PublishedProducts;
use App\SubscriptionRepository\UserSubscriptions;
use App\ProductsRepository\SubscribedProduct;

class WorkoutController extends Controller
{

    private $allAccess;

    private $products;

    private $userSubscriptions;

    private $isSubscribedProduct;

    public function __construct(
                                SubscribedAllAccess $allAccess,
                                PublishedProducts $products, 
                                UserSubscriptions $userSubscriptions,
                                SubscribedProduct $isSubscribedProduct)
    {
        $this->middleware('auth');

        $this->middleware('subscribed');

        $this->allAccess = $allAccess; // all access subscription

        $this->products = $products;

        $this->userSubscriptions = $userSubscriptions;

        $this->isSubscribedProduct = $isSubscribedProduct;
    }


    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user_id = Auth::id();

        $products = $this->products->getAllPublishedProducts(); 
        
        $isAllAccessSubscribed = $this->allAccess->getAllPoductsOnAccessAll(); // either false or list of all products if subscribed all access
       
        // //check first for all access subscription
        if(($isAllAccessSubscribed) != false){
           
            return view('workout.index',[
                'products' => $isAllAccessSubscribed
            ]);
        } // return as the user has subscribed to all access
       

        $subscribed_products = $this->userSubscriptions->subscribed_products(); // for multiple susbcrition
     
        if(($subscribed_products) != ''){
               
            return view('workout.index',[
                'products' => $subscribed_products
            ]);
        }


        if($subscribed_products == false){ // user has unsubscribed and grce period ends too
            return redirect('settings#/subscription'); 
        }else{
            // pass the list of purchased products
            return view('workout.index',[
                'products' => $subscribed_products
            ]);
        }
    }

    protected function scripts($product_id, $category_id){
        // //check first for all access subscription
        $isAllAccessSubscribed = $this->allAccess->getAllPoductsOnAccessAll(); // either false or list of all products if subscribed all access
       

        $verfied = $$this->isSubscribedProduct->verify($product_id);; // verify whether the user has purchased this product or not 
        $user_id = Auth::id();
       

        if($verfied || $isAllAccessSubscribed != false){
            //get the picture of the category and pass that to view 

            $workout_scripts = DB::table('workout_scripts')
                            ->where('workout_category_id',$category_id)
                            ->get();// get all the categories of this product
            $total_scripts = count($workout_scripts);
            
            $category = WorkoutCategory::where('id',$category_id)->get();
            //dd($category[0]);
            return view('workout.scripts',[
                'category' => $category[0],
                'total_scripts' => $total_scripts,
                'workout_scripts' => $workout_scripts
            ]);   
        }else{
            return redirect('settings#/subscription'); 
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Workout  $workout
     * @return \Illuminate\Http\Response
     */
    public function show($product_id)
    {

        // //check first for all access subscription
        $isAllAccessSubscribed = $this->allAccess->getAllPoductsOnAccessAll(); // either false or list of all products if subscribed all access
       
        
        $verfied = $this->isSubscribedProduct->verify($product_id);; // verify whether the user has purchased this product or not 
       
       
        $user_id = Auth::id();
      
        
        if($verfied || $isAllAccessSubscribed != false){ // check whether the user has subscribed to the requested product or has all access susbcription
            $licence  = Product::where('id', $product_id)->pluck('name');
            $product_categories = ProductWorkoutCategory::where('product_id',$product_id)->get();
            foreach ($product_categories as $category) {
                $categories[] = DB::table('workout_categories')->where('id', $category->workout_category_id)->get();// get all the categories of this product
            }
            //dd($categories);
            return view('workout.show',[
                 'categories' => $categories,
                 'licence' => $licence[0],
                 'product_id' => $product_id
            ]);
        }else{
            return redirect('settings#/subscription'); 
        }
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Workout  $workout
     * @return \Illuminate\Http\Response
     */
    public function edit(Workout $workout)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Workout  $workout
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Workout $workout)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Workout  $workout
     * @return \Illuminate\Http\Response
     */
    public function destroy(Workout $workout)
    {
        //
    }



   
}
