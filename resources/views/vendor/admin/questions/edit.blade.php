<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>{{ Admin::title() }}</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

    <link rel="stylesheet" href="{{ asset("/vendor/laravel-admin/AdminLTE/bootstrap/css/bootstrap.min.css") }}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ asset("/vendor/laravel-admin/font-awesome/css/font-awesome.min.css") }}">

    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset("/vendor/laravel-admin/AdminLTE/dist/css/skins/" . config('admin.skin') .".min.css") }}">

    {!! Admin::css() !!}
    <link rel="stylesheet" href="{{ asset("/vendor/laravel-admin/laravel-admin/laravel-admin.css") }}">
    <link rel="stylesheet" href="{{ asset("/vendor/laravel-admin/nprogress/nprogress.css") }}">
    <link rel="stylesheet" href="{{ asset("/vendor/laravel-admin/sweetalert/dist/sweetalert.css") }}">
    <link rel="stylesheet" href="{{ asset("/vendor/laravel-admin/nestable/nestable.css") }}">
    <link rel="stylesheet" href="{{ asset("/vendor/laravel-admin/toastr/build/toastr.min.css") }}">
    <link rel="stylesheet" href="{{ asset("/vendor/laravel-admin/bootstrap3-editable/css/bootstrap-editable.css") }}">
    <link rel="stylesheet" href="{{ asset("/vendor/laravel-admin/google-fonts/fonts.css") }}">
    <link rel="stylesheet" href="{{ asset("/vendor/laravel-admin/AdminLTE/dist/css/AdminLTE.min.css") }}">

    <!-- REQUIRED JS SCRIPTS -->
    <script src="{{ asset ("/vendor/laravel-admin/AdminLTE/plugins/jQuery/jQuery-2.1.4.min.js") }}"></script>
    <script src="{{ asset ("/vendor/laravel-admin/AdminLTE/bootstrap/js/bootstrap.min.js") }}"></script>
    <script src="{{ asset ("/vendor/laravel-admin/AdminLTE/plugins/slimScroll/jquery.slimscroll.min.js") }}"></script>
    <script src="{{ asset ("/vendor/laravel-admin/AdminLTE/dist/js/app.min.js") }}"></script>
    <script src="{{ asset ("/vendor/laravel-admin/jquery-pjax/jquery.pjax.js") }}"></script>
    <script src="{{ asset ("/vendor/laravel-admin/nprogress/nprogress.js") }}"></script>

    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body class="hold-transition {{config('admin.skin')}} {{join(' ', config('admin.layout'))}}">
<div class="wrapper">

    @include('admin::partials.header')

    @include('admin::partials.sidebar')

    <div class="content-wrapper" id="pjax-container">
        @yield('content')
       
        <div class="row">
            <div class="col-sm-12">
                <h2>Update a Question</h2>
            </div>

            <div class="col-sm-12">

            @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
            @endif


                <form role="form" method="POST" action="update" enctype="multipart/form-data"> 
                @csrf
                    <div class="box-body">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Text</label>
                            <input type="text" class="form-control" value="{{$question->text}}" id="exampleInputEmail1" placeholder="Name" name="text">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Select Quiz</label>
                            <select class="form-control select2" style="width: 100%;" name="quiz_id">
                                @foreach($quizzes as $quiz)
                                    <option <?php if($quiz->id == $question->quiz_id){ ?> selected="selected" <?php } ?> value="{{$quiz->id}}">{{$quiz->title}}</option> 
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Option: A</label>
                            <input type="text" class="form-control" value="{{$options->A}}" id="exampleInputEmail1" placeholder="Option" name="options[]">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Option: B</label>
                            <input type="text" class="form-control" value="{{$options->B}}" id="exampleInputEmail1" placeholder="Option" name="options[]">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Option: C</label>
                            <input type="text" class="form-control" value="{{$options->C}}" id="exampleInputEmail1" placeholder="Option" name="options[]">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Option: D</label>
                            <input type="text" class="form-control" value="{{$options->D}}" id="exampleInputEmail1" placeholder="Option" name="options[]">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Correct Option</label>
                            <select class="form-control select2" style="width: 100%;" name="answer">
                                <option value="A"   <?php if($question->answer === 'A'){ ?> selected="selected" <?php } ?> >Option: A</option>
                                <option value="B" <?php if($question->answer === 'B'){?> selected="selected" <?php } ?> >Option: B</option>
                                <option value="C" <?php if($question->answer === 'C'){?> selected="selected" <?php }?> >Option: C</option>
                                <option value="D" <?php if($question->answer === 'D'){?> selected="selected" <?php } ?> > Option: D</option> 
                            </select>
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>
             </div>
        </div><!-- row ended-->

        {!! Admin::script() !!}
    </div>

    @include('admin::partials.footer')

</div>

<!-- ./wrapper -->

<script>
    function LA() {}
    LA.token = "{{ csrf_token() }}";
</script>

<!-- REQUIRED JS SCRIPTS -->
<script src="{{ asset ("/vendor/laravel-admin/nestable/jquery.nestable.js") }}"></script>
<script src="{{ asset ("/vendor/laravel-admin/toastr/build/toastr.min.js") }}"></script>
<script src="{{ asset ("/vendor/laravel-admin/bootstrap3-editable/js/bootstrap-editable.min.js") }}"></script>
<script src="{{ asset ("/vendor/laravel-admin/sweetalert/dist/sweetalert.min.js") }}"></script>
{!! Admin::js() !!}
<script src="{{ asset ("/vendor/laravel-admin/laravel-admin/laravel-admin.js") }}"></script>

</body>
</html>
