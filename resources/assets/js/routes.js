import VueRouter from 'vue-router';

var routes = [
    { path: '*', redirect: '/' },
    {
        path: '/',
        component: require('./components/ui/Dashboard'),
        meta: {

        }
    },
    {
        path: '/training/:p/:q',
        component: require('./components/ui/Training'),
        meta: {

        }
    },
    {
        path: '/tools/:p',
        component: require('./components/ui/Tools'),
        meta: {

        }
    },
    {
        path: '/branding/:p',
        component: require('./components/ui/Branding'),
        meta: {

        }
    },
    {
        path: '/scripts/:p',
        component: require('./components/ui/scripts'),
        meta: {

        }
    },

];

export default new VueRouter({
    routes: routes
});