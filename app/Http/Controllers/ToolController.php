<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tool;
use App\Product;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\DB;
use Laravel\Cashier\Subscription;
use App\AllAccess\SubscribedAllAccess;
use App\ProductsRepository\PublishedProducts;
use App\SubscriptionRepository\UserSubscriptions;
use App\ProductsRepository\SubscribedProduct;

class ToolController extends Controller
{
    private $allAccess;
    
    private $products;

    private $userSubscriptions;

    private $isSubscribedProduct;

    public function __construct(
                                SubscribedAllAccess $allAccess,
                                PublishedProducts $products, 
                                UserSubscriptions $userSubscriptions,
                                SubscribedProduct $isSubscribedProduct
                                )
    {
        $this->middleware('auth');

        $this->middleware('subscribed');

        $this->allAccess = $allAccess; // all access subscription

        $this->products = $products;

        $this->userSubscriptions = $userSubscriptions;

        $this->isSubscribedProduct = $isSubscribedProduct;
    }

    public function presentation($name){
        
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user_id = Auth::id();
        $products = $this->products->getAllPublishedProducts(); // all active/published products
        
        $isAllAccessSubscribed = $this->allAccess->getAllPoductsOnAccessAll(); // either false or list of all products if subscribed all access
       
        // //check first for all access subscription
        if(($isAllAccessSubscribed) != false){
           
            return view('tool.index',[
                'products' => $isAllAccessSubscribed
            ]);
        } // return as the user has subscribed to all access


        
        //$subscribed_product[] = $this->subscribed_product($user_id);

        $subscribed_products = $this->userSubscriptions->subscribed_products(); // for multiple susbcrition
        
     
         // if(($subscribed_product) != ''){
        //     $products = []; // return an empty array to the view
        // }
        if(($subscribed_products) != ''){

            return view('tool.index',[
                'products' => $subscribed_products
            ]);
        }

        if($subscribed_products[0] == false){ // user has unsubscribed and grce period ends too
            return redirect('settings#/subscription'); 
        }else{
            return view('tool.index',[
                'products' => $subscribed_products
            ]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // //check first for all access subscription
        $isAllAccessSubscribed = $this->allAccess->getAllPoductsOnAccessAll(); // either false or list of all products if subscribed all access
      

        // check whether has purchased this product
    
        $tools= $obj= Tool::where('product_id', $id)->get(); 
        
        $licence = Product::where('id', $id)->pluck('name');
        
        // Verify whether the user can access this resource or not i.e if purchased
        $verified = $this->isSubscribedProduct->verify($id); // check if the user has subscribed to this product
   

        if($verified && count($tools) || $isAllAccessSubscribed != false){
            return view('tool.show',[
                'tools' => $tools,
                'licence' => $licence[0]
            ]);
        }else{
            return redirect('settings#/subscription'); 
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


}
