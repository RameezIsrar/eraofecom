@extends('master')
@section('title', $licence . ' Branding')
@section('page-title')
  @yield('title')
@endsection


@section('page-content')

@push('scripts')

  <div class="row">

  @foreach ($brandings as $branding)    

    <div class="col-sm-4">
      <div class="card">
        <div class="card-body">
          <h5 class="card-title">{{$branding->title}}</h5>
          @if($branding->type == "certificate")
            <a href="/branding/my_certificate/{{$product_id}}" target="_blank" class="btn btn-primary">Download Now</a>
          @else
            <a href="/branding/download/{{$product_id}}/{{$branding->id}}" target="_blank" class="btn btn-primary">Download Now</a>
          @endif
        </div>
      </div>
    </div>

  @endforeach

  </div>

@endsection
