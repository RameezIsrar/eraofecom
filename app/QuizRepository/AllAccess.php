<?php

namespace App\QuizRepository;

class AllAccess
{

    public static function get($count)
    { // get the user's subcribed products

        $locked_quests = ['', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''];

        for ($i = $count; $i < 16; $i++) {
            $locked_quests[$i] = "<span class='badge badge-pill badge-secondary'>All Access</span>";
        }

        return $locked_quests;

    }

}
