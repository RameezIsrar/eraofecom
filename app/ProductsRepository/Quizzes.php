<?php

namespace App\ProductsRepository;

use Illuminate\Http\Request;
use App\Product;
use Illuminate\Support\Facades\DB;
use Illuminate\Routing\Redirector;
use App\Quiz;

class Quizzes{

    function __construct() {
       
    }


    public static function get($productId){

        $product_quests = Quiz::where('product_id', $productId) //here the return value is actually the level i.e 1.2,1.3 of the quiz id from the quiz table
                                 ->pluck('id');
        return $product_quests;
    }



}



