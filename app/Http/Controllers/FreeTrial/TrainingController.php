<?php

namespace App\Http\Controllers\FreeTrial;

use App\AllAccess\SubscribedAllAccess;
use App\ProductsRepository\Name as ProductName;
use App\ProductsRepository\ProductQuest;
use App\ProductsRepository\PublishedProducts;
use App\ProductsRepository\Quizzes;
use App\ProductsRepository\SubscribedProduct;
use App\QuizRepository\Content;
use App\QuizRepository\IsPassed;
use App\QuizRepository\IsWatched;
use App\QuizRepository\Level;
use App\QuizRepository\Locked;
use App\QuizRepository\Passed;
use App\QuizRepository\Video;
use App\Repository\Questions\Answers;
use App\Repository\Questions\RandomFive;
use App\SubscriptionRepository\UserSubscriptions;
use App\Tasks\Progress;
use App\Tasks\QuestAccess;
use App\Tasks\StartQuizAccess;
use App\UserTrainingHistory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Tasks\IsUnderFreeTrial;
use App\QuizRepository\IsFree as isQuizFree;
use App\QuizRepository\AllAccess as UpgradeToAllAccess;

class TrainingController extends Controller
{

    private $allAccess;

    private $products;

    private $userSubscriptions;

    private $isSubscribedProduct;

    public function __construct(
        UserTrainingHistory $UserTraininghHstory,
        SubscribedAllAccess $allAccess,
        PublishedProducts $products,
        UserSubscriptions $userSubscriptions,
        SubscribedProduct $isSubscribedProduct) {

        $this->middleware('auth');

        $this->UserTraininghHstory = $UserTraininghHstory;

        $this->allAccess = $allAccess; // all access subscription

        $this->products = $products;

        $this->userSubscriptions = $userSubscriptions;

        $this->isSubscribedProduct = $isSubscribedProduct;

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user_id = Auth::id();

        $products = $this->products->getAllPublishedProducts();


        $isAllAccessSubscribed = $this->allAccess->getAllPoductsOnAccessAll(); // either false or list of all products if subscribed all access

        // //check first for all access subscription
        if (($isAllAccessSubscribed) != false) {

            return view('free.training.index', [
                'products' => $isAllAccessSubscribed,
            ]);
        } // return as the user has subscribed to all access

        $subscribed_products = $this->userSubscriptions->subscribed_products(); // for multiple susbcrition

        if (($subscribed_products) != '') {
            //  list of all purchased products should be sent to this view
            return view('free.training.index', [
                'products' => $subscribed_products,
            ]);
        }

        if ($subscribed_product[0] == false) { // user has unsubscribed and grce period ends too
            return redirect('settings#/subscription');
        } else {

            //  list of all purchased products should be sent to this view
            return view('free.training.index', [
                'products' => $subscribed_product,
            ]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($productId, $questId = 1.1, Request $request)
    {

        $isUnderFreeTrial  = isUnderFreeTrial::verify();
        
        // if the user is under free trial
        if ($isUnderFreeTrial) {

            $productName = ProductName::get($productId);

            if ($questId == 1.1) {

                $questId = ProductQuest::Id($productId);

                $product_quests = Quizzes::get($productId); // get all the quests id's of the product

                return redirect("/free/training/$productId/$questId");

            } // if the quiz/quest arent submitied with request
          
            $reqQuestId = $questId;

            if ($questId != 1.1) {

                $questId = Level::get($questId);

            }

            $product_quests = Quizzes::get($productId); // get all the quests id's of the product

            $verified = isQuizFree::verify($productId,$questId);
         
            if ($verified) { // check whether the current logged in user has the access of this specified id -- from model

                $request->session()->put('trainingProductId', $productId);
                $request->session()->put('trainingProductQuestId', $questId);

                //get the quest progress
                $progress = Progress::result($productId);

                //check whether the user should be asked to begin quiz or not
                $already_passed = IsPassed::verify($productId, $questId);

                // check whether the user has already watched the video or not
                $already_watched = IsWatched::verify($productId, $questId);

                //initialize passed quests array
                $passed_quests = Passed::get($productId); // get the passed quests/quizzes

                $locked_quests = UpgradeToAllAccess::get(Passed::$count); // pass the Static property to locked class

                $quest_level = Level::get($reqQuestId);

                $video_link = Video::get($productId, $questId);
                $videoContent = Content::get($productId, $questId);

                $questId = ProductQuest::Id($productId, $questId);
                return view('free.training.show', [
                    'video_link' => $video_link,
                    'product_id' => $productId,
                    'questId' => $questId,
                    'passed_quests' => $passed_quests,
                    'locked_quests' => $locked_quests,
                    'already_passed' => $already_passed,
                    'already_watched' => $already_watched,
                    'videoContent' => $videoContent,
                    'progress' => $progress,
                    'product_quests' => $product_quests,
                    'quest_level' => $quest_level,
                    'productName' => $productName,
                ]);
            } else {
                $questId = ProductQuest::Id($productId);
                $request->session()->flash('error', "You can only access this phase by passing the previous one by securing 60% score");
                return redirect("/training/$productId/$questId");
            }

        } else {
            return redirect('settings#/subscription');
          
        } //baseverified

    }

    public function startQuiz($productId, $questId, $name = "start-quiz", Request $request)
    {
        $reqQuestId = $questId;

        $user_id = Auth::id();
        $questId = Level::get($questId); // the return value is the questlevel i.e 1.2,3.1 etc
        $verified = StartQuizAccess::verify($productId, $questId);

        if ($verified) { // verified

            $productName = ProductName::get($productId); // get the product name

            $request->session()->put('trainingProductId', $productId);
            $request->session()->put('trainingProductQuestId', $questId);

            $questions = RandomFive::get($reqQuestId); // get 5 random questions

            $correctAnswers = Answers::get($questions);

            $request->session()->put('correctAnswers', $correctAnswers);

            return view('free.training.startQuiz', [
                'questions' => $questions,
                'productName' => $productName,
                'questId' => $questId,
            ]);
        } else {
            $questId = ProductQuest::Id($productId);
            $request->session()->flash('error', "You can only access this phase by passing the previous one by securing 60% score");
            return redirect("/training/$productId/$questId");
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

}
