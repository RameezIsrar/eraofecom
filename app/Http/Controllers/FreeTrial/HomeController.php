<?php

namespace App\Http\Controllers\FreeTrial;

use App\AllAccess\SubscribedAllAccess;
use App\ProductsRepository\PublishedProducts;
use App\ProductsRepository\SingleProduct;
use App\ProductsRepository\SubscribedProduct;
use App\SubscriptionRepository\UserSubscriptions;
use App\Tasks\Progress;
use App\Tasks\Score;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    private $allAccess;

    private $products;

    private $userSubscriptions;

    private $isSubscribedProduct;

    public function __construct(
        SubscribedAllAccess $allAccess,
        PublishedProducts $products,
        UserSubscriptions $userSubscriptions,
        SubscribedProduct $isSubscribedProduct) {

        $this->middleware('auth');

        $this->allAccess = $allAccess; // all access subscription

        $this->products = $products;

        $this->userSubscriptions = $userSubscriptions;

        $this->isSubscribedProduct = $isSubscribedProduct;
    }

    /**
     * Show the application dashboard.
     *
     * @return Response
     */
    public function index()
    {

        $user_id = Auth::id();
        $products = $this->products->getAllPublishedProducts(); // pass all the purchased products of a user

        $isAllAccessSubscribed = $this->allAccess->getAllPoductsOnAccessAll(); // either false or list of all products if subscribed all access

        if (($isAllAccessSubscribed) != false) {

            return view('dashboard.home', [
                'products' => $isAllAccessSubscribed,
            ]);
        } // return as the user has subscribed to all access

        $subscribed_products = $this->userSubscriptions->subscribed_products(); // for multiple susbcrition

        if (($subscribed_products) != '') {
            return view('dashboard.home', [
                'products' => $subscribed_products,
            ]);
        } // if the user has subscribed to one or multple products but not all access

        if ($subscribed_product == false) { // user has unsubscribed and grce period ends too
            return redirect('settings#/subscription');
        } else {
            return view('dashboard.home', [
                'products' => $subscribed_products,
            ]);
        }
    }

    public function show($productId)
    {
        //dd(Auth::user()->sparkPlan());
        // dd(Auth::user()->current_billing_plan);
        $user_id = Auth::id();

        $verfied = $this->isSubscribedProduct->verify($productId); // check if the user has subscribed to this product

        $isAllAccessSubscribed = $this->allAccess->getAllPoductsOnAccessAll();

        //check whether requested  the product has subscribed
        //or check if the allAccess is subscribed
        // if ($verfied || $isAllAccessSubscribed == true) {

            $product = SingleProduct::get($productId);

            $progress = Progress::result($productId);

            $score_array = Score::result($productId);

            return view('dashboard.show', [
                'product' => $product[0],
                'progress' => $progress,
                'score_array' => $score_array,
            ]);

        // } else {
        //     return redirect('settings#/subscription');
        // }
    }

}
