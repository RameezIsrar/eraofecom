<?php

namespace App\Admin\Controllers;

use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;
use App\Branding;
use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector;
use App\ProductsRepository\AllProducts;
use App\UserTrainingHistory;
use App\User;

class ScoreController extends Controller
{
    use ModelForm;

    private $products;

    public function __construct(AllProducts $products)
    {
        
        $this->products = $products;
        
    }

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        
        return Admin::content(function (Content $content) {

            $content->header('Score Board');
            $content->description('List');

            $content->body($this->grid());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        // return Admin::content(function (Content $content) use ($id) {

        //     $content->header('header');
        //     $content->description('description');

        //     $content->body($this->form()->edit($id));
        // });

        // $branding= Branding::where('id', $id)->get();
        // $products= $this->products->allProducts(); 
        
        // return view('vendor/admin/branding.edit',[
        //     'branding' => $branding[0],
        //     'products' => $products
        // ]);

    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        // return Admin::content(function (Content $content) {

        //     $content->header('header');
        //     $content->description('description');

        //     $content->body($this->form());
        // });

        //alpha
        // $products= $this->products->allProducts();
         
        // return view('vendor/admin/branding.create',[
        //     'products' => $products
        // ]);

    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(UserTrainingHistory::class, function (Grid $grid) {

            $grid->disableCreateButton();
            $grid->disableActions();

            $grid->id('ID')->sortable();
            
            $grid->user_id()->display(function($user_id) {
                return User::find($user_id)->email;
            }); 

            $grid->product_id()->display(function($product_id) {
                return Product::find($product_id)->name;
            });
            
            $grid->quest_level('Phase')->sortable();

            $grid->score('score')->sortable();

            
            
            $grid->created_at();
            $grid->updated_at();

            $grid->filter(function ($filter) {

                // Sets the range query for the created_at field
                $filter->between('created_at', 'Created Time')->datetime();

                $filter->equal('product_id','Product ID');
            });


            $grid->filter(function ($filter) {

               
                $filter->equal('user_id','User ID');
            });


            $grid->filter(function ($filter) {

                $filter->like('quest_level','Phase/Level');
            });

            $grid->filter(function ($filter) {

                $filter->like('score','Score');
            });


        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(UserTrainingHistory::class, function (Form $form) {

            $form->display('id', 'ID');

            $form->text('name', 'Name');
            $form->display('created_at', 'Created At');
            $form->display('updated_at', 'Updated At');
        });
    }

    public function store(Request $request){

    }

    public function update(Request $request, $id){
      
        
    }
}
