let mix = require('laravel-mix');
let exec = require('child_process').exec;
let path = require('path');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix
    .sass('resources/assets/sass/app.scss', 'public/css')
    .js('resources/assets/js/app.js', 'public/js')
    .copy('node_modules/sweetalert/dist/sweetalert.min.js', 'public/js/sweetalert.min.js')
    .sass('resources/assets/sass/app-rtl.scss', 'public/css')
    .then(() => {
        exec('node_modules/rtlcss/bin/rtlcss.js public/css/app-rtl.css ./public/css/app-rtl.css');
    })
    .version(['public/css/app-rtl.css'])
    .webpackConfig({
        resolve: {
            modules: [
                path.resolve(__dirname, 'vendor/laravel/spark-aurelius/resources/assets/js'),
                'node_modules'
            ],
            alias: {
                'vue$': 'vue/dist/vue.js'
            }
        }
    });

    mix.sass('resources/assets/sass/custom.scss', 'public/css')
        .version();

    mix.sass('resources/assets/sass/checkout.scss', 'public/css')
        .version();
        
    mix.sass('resources/assets/sass/login.scss', 'public/css')
        .version();

    mix.sass('resources/assets/sass/thankyou.scss', 'public/css')
        .version();

    mix
        .js('resources/assets/js/custom.js', 'public/js')
        .version();

    mix
        .js('resources/assets/js/tracking-codes/checkout.js', 'public/js/tracking-codes')
        .version();

    mix
        .js('resources/assets/js/tracking-codes/thankyou.js', 'public/js/tracking-codes')
        .version();

    mix
        .js('resources/assets/js/tracking-codes/gtm.js', 'public/js/tracking-codes')
        .version();

    mix
        .js('resources/assets/js/custom-app.js', 'public/js')
        .version();

    mix
        .js('resources/assets/js/vuetify.js', 'public/js')
        .version();
    
    mix
        .sass('resources/assets/sass/ui.scss', 'public/css')
        .version();